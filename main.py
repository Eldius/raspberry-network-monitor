
import logging

from tools import mock_data, display_data as display, network_test as test
from tools.persistence.persistence import persist, listAll
from tools.config import logging_config

logging.info("Starting test...")

#results_dict = mock_data.get_network_mock_data()

logging.info("Test finished.")

logging.info("Listing all results:")
display.show_result(listAll())

display.show_result(persist(test.execute_network_test()))

