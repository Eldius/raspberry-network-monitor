import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']
).get_hosts('all')


def test_install_folder(host):
    f = host.file('/usr/local/network-monitor')

    assert f.exists
    assert f.user == 'root'
    assert f.group == 'root'
    assert f.is_directory


def test_service_config_files(host):
    service = host.file("/etc/systemd/system/network-monitor.service")
    timer = host.file("/etc/systemd/system/network-monitor.timer")

    assert service.exists
    assert service.user == 'root'
    assert service.group == 'root'
    assert service.is_file

    assert timer.exists
    assert timer.user == 'root'
    assert timer.group == 'root'
    assert timer.is_file
