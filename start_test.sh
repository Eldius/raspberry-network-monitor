#/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
CURR_DIR="${PWD}"

cd ${SCRIPT_DIR}

function prepare_env {
    virtualenv -p python3 ${SCRIPT_DIR}/.venv
    source ${SCRIPT_DIR}/.venv/bin/activate
    pip install -r ${SCRIPT_DIR}/requirements.txt
}

[[ "x${UPDATE_BEFORE_RUN}" -eq "x1" ]] && git pull

[ ! -d ${SCRIPT_DIR}/.venv ] && prepare_env

source ${SCRIPT_DIR}/.venv/bin/activate

python ${SCRIPT_DIR}/main.py
