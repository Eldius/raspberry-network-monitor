

def get_network_mock_data():
    return {
        "download": 47901149.69467199
        , "upload": 9363916.832105564
        , "ping": 14.704
        , "server": {
            "url": "http://test.afinet.com.br:8080/speedtest/upload.php"
            , "lat": "-22.7858"
            , "lon": "-43.3119"
            , "name": "Duque de Caxias"
            , "country": "Brazil"
            , "cc": "BR"
            , "sponsor": "AFINET"
            , "id": "22448"
            , "host": "test.afinet.com.br:8080"
            , "d": 15.057214026458773
            , "latency": 14.704
        }
        , "timestamp": "2019-08-19T19:38:04.825297Z"
        , "bytes_sent": 12091392
        , "bytes_received": 60334115
        , "share": "http://www.speedtest.net/result/8515224349.png"
        , "client": {
            "ip": "177.192.171.226"
            , "lat": "-22.9201"
            , "lon": "-43.3307"
            , "isp": "NET Virtua"
            , "isprating": "3.7"
            , "rating": "0"
            , "ispdlavg": "0"
            , "ispulavg": "0"
            , "loggedin": "0"
            , "country": "BR"
        }
    }