
from tools.config.app_config import get_database_folder
from tools.persistence.persistence_utils import DateTimeSerializer, DateSerializer, TimeDeltaSerializer, DatetimeCacheTable
from tinydb import TinyDB, Query
from pathlib import Path
import os

from datetime import datetime
from tinydb_serialization import SerializationMiddleware

home_folder = str(Path.home())
config_folder = get_database_folder()
data_file=f"{config_folder}/network_monitor.json"

Path(config_folder).mkdir(parents=True, exist_ok=True)

#TinyDB.table_class = DatetimeCacheTable

serialization = SerializationMiddleware()
serialization.register_serializer(DateTimeSerializer(), 'TinyDateTime')
#serialization.register_serializer(DateSerializer(), 'TinyDate')
#serialization.register_serializer(TimeDeltaSerializer(), 'TinyTimeDelta')

db = TinyDB(data_file, default_table='network_data', storage=serialization)

#db = TinyDB(data_file, default_table='network_data')

def persist(value):
    db.insert(value)
    return value

def listAll():
    return db.all()
