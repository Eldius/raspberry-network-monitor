
import logging

import cfg_load as loader

from cfg_loader import BaseConfigLoader

from pathlib import Path

from tools.config.config_model import MonitorConfiguration

home_folder=str(Path.home())

config_folder=f"{home_folder}/.monitor"
config_file = f"{config_folder}/monitor_config.yml"
substitution_mapping = {"HOME": home_folder}

raw_config = loader.load(config_file, load_raw=True)

my_config_loader = BaseConfigLoader(MonitorConfiguration, substitution_mapping=substitution_mapping)
config = my_config_loader.load(raw_config["config"])

def get_database_folder():
    print(f"database_folder: {config.get('database_folder')}")
    return config.get("database_folder")

def get_network_constraint():
    return config.get("network").get("constraint")

def get_app_config():
    return config
