from tools.config import app_config

import logging

config = app_config.get_app_config()

# create logger
logging.basicConfig(format=config.get("logging_format"), level=logging.getLevelName(config.get("logging_level")))
