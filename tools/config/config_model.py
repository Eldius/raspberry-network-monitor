
from marshmallow import fields
from cfg_loader import ConfigSchema

class NetworkConstraint(ConfigSchema):
    min_download = fields.Int(missing=40)
    min_upload = fields.Int(missing=8)
    enabled = fields.Boolean(missing=True)
    max_ping = fields.Int(missing=20)

class NetworkConfig(ConfigSchema):
    constraint = fields.Nested("NetworkConstraint")

class MonitorConfiguration(ConfigSchema):
    debug = fields.Boolean(missing=False)
    database_folder = fields.Str(required=True)
    network = fields.Nested('NetworkConfig')
    logging_dateformat = fields.Str(missing='%m/%d/%Y %I:%M:%S %p')
    logging_format = fields.Str(missing='%(asctime)s %(message)s')
    logging_level = fields.Str(missing="NOTSET")
