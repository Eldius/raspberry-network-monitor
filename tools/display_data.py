
import json
import os

try:
    output_file = open("/dev/tty0", 'wt')
except:
    output_file = open(os.ttyname(0), 'wt')

def convert_to_megabits(value):
    return value / (1024 * 1024)

def show_result(result):
    if type(result) == list:
        for r in result:
            show_result(r)
    else:
        print(f"""---
        - download is ok?   {result["download_ok"]}
        - upload is ok?     {result["upload_ok"]}
        - ping is ok?       {result["ping_ok"]}
        - date_time:        {result["date_time"]}
        - timestamp:        {result["timestamp"]}
        - ping:             {result["ping"]}
        - download:         {convert_to_megabits(result["download"])} Mb/s / {result["min_download"]} Mb/s [{result["download"]} b/s]
        - upload:           {convert_to_megabits(result["upload"])} Mb/s / {result["min_upload"]} Mb/s [{result["upload"]} b/s]
        """
        , file=output_file
        )
    return result
