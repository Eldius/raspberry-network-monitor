
import speedtest
import datetime

import datetime
import dateutil.parser

from tools.config.app_config import get_network_constraint

def getDateTimeFromISO8601String(s):
    return dateutil.parser.parse(s)

def convert_to_megabits(value):
    return value / (1024 * 1024)

def execute_network_test():

    config = get_network_constraint()

    servers = []
    # If you want to test against a specific server
    # servers = [1234]

    threads = None
    # If you want to use a single threaded test
    # threads = 1

    s = speedtest.Speedtest()
    s.get_servers(servers)
    s.get_best_server()
    s.download(threads=threads)
    s.upload(threads=threads)
    s.results.share()

    test_result = s.results.dict()

    test_result["date_time"] = getDateTimeFromISO8601String(test_result["timestamp"])

    test_result["download_em_megabytes"] = convert_to_megabits(test_result["download"])
    test_result["upload_em_megabytes"] = convert_to_megabits(test_result["upload"])

    if config.get("enabled"):
        test_result["download_ok"] = config.get("min_download") < test_result["download_em_megabytes"]
        test_result["upload_ok"] = config.get("min_upload") < test_result["upload_em_megabytes"]
        test_result["ping_ok"] = config.get("max_ping") < test_result["ping"]
        test_result["min_download"] = config.get("min_download")
        test_result["min_upload"] = config.get("min_upload")
        test_result["max_ping"] = config.get("max_ping")

    if config.get("debug"):
        print(test_result)

    return test_result
